package io.github.kylesferrazza.treasurechest;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

/**
 * Created by Kyle on 12/4/2014.
 */
public class FireworkTimer implements Runnable {

    private Main main;
    private List<Location> fireworks;
    private int lastY;
    private boolean done = false;
    private TreasureChest treasureChest;

    public FireworkTimer(Main main, List<Location> fireworks, int lastY, TreasureChest treasureChest) {
        this.fireworks = fireworks;
        this.lastY = lastY;
        this.main = main;
        this.treasureChest = treasureChest;
    }

    @Override
    public void run() {
        for (Location location : fireworks) {
            FireworkEffect.Type type = FireworkEffect.Type.BURST;
            if (lastY == location.getBlockY()) {
                type = FireworkEffect.Type.BALL_LARGE;
            }

            Bukkit.getScheduler().runTask(main, new DoFireWorkTask(location, type, treasureChest.getFireworkColor()));
            try {
                Thread.sleep(190L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (lastY == location.getBlockY()) {
                Bukkit.getScheduler().runTask(main, new ChestSpawner(treasureChest));
                done = true;
                break;
            }
        }
    }

    public boolean isDone() {
        return done;
    }
}
