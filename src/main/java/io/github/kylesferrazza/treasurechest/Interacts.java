package io.github.kylesferrazza.treasurechest;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kyle on 12/7/2014.
 */
public class Interacts implements Listener {
    private static List<TreasureChest> chestList = new ArrayList<TreasureChest>();

    public static void addChest(TreasureChest treasureChest) {
        chestList.add(treasureChest);
    }

    public static void removeChest(TreasureChest treasureChest) {
        chestList.remove(treasureChest);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (event.getClickedBlock() == null) {
            return;
        }
        Location interLoc = event.getClickedBlock().getLocation();
        for (TreasureChest treasureChest : chestList) {
            for (Location loc : treasureChest.getOriginalBlockLocs()) {
                if (loc.equals(interLoc)) {
                    event.setCancelled(true);
                    return;
                }
            }
        }
    }

    public static boolean locsGood(List<Location> allLocs) {
        for (Location location : allLocs) {
            for (TreasureChest treasureChest : chestList) {
                for (Location listLoc : treasureChest.getAllLocs()) {
                    if (location.equals(listLoc)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

}
