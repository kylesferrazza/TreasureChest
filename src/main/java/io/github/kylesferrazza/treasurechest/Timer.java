package io.github.kylesferrazza.treasurechest;

import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Kyle Sferrazza on 11/15/2014.
 * This file is a part of: TreasureChest.
 * All rights reserved.
 */
public class Timer implements Runnable {

    TreasureChest tc;

    public Timer(TreasureChest tc) {
        this.tc = tc;
    }

    @Override
    public void run() {
        tc.endCycle();
    }
}
