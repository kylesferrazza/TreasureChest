package io.github.kylesferrazza.treasurechest;

import net.minecraft.server.v1_8_R1.EntityFireworks;
import org.bukkit.Color;

import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R1.inventory.CraftItemStack;
import org.bukkit.entity.EntityType;

import org.bukkit.entity.Firework;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;

import java.util.Random;

/**
 * Created by Kyle on 12/6/2014.
 */
public class DoFireWorkTask implements Runnable {
    private Location loc;
    private Color fireworkColor;
    private FireworkEffect.Type type;

    public DoFireWorkTask(Location loc, FireworkEffect.Type type, Color fireworkColor) {
        this.loc = loc;
        this.type = type;
        this.fireworkColor = fireworkColor;
    }

    @Override
    public void run() {
        ItemStack fws = new ItemStack(Material.FIREWORK);
        FireworkMeta meta = (FireworkMeta) fws.getItemMeta();
        meta.addEffect(FireworkEffect.builder()
                .with(type)
                .withColor(fireworkColor)
                .trail(true)
                .flicker(false)
                .build());
        fws.setItemMeta(meta);
        EntityFireworks fw = new EntityFireworks(((CraftWorld)
                loc.getWorld()).getHandle(),
                loc.getX(),
                loc.getY(),
                loc.getZ(),
                CraftItemStack.asNMSCopy(fws));
        ((CraftWorld) loc.getWorld()).getHandle().addEntity(fw);
        ((Firework) fw.getBukkitEntity()).detonate();
    }

}
