package io.github.kylesferrazza.treasurechest;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kyle Sferrazza on 11/15/2014.
 * This file is a part of: TreasureChest.
 * All rights reserved.
 */
public class ChestSpawnEffect extends BukkitRunnable {
    private TreasureChest treasureChest;
    private Location chestLocation;
    private Color fireworkColor;

    private Main main;

    public ChestSpawnEffect(TreasureChest treasureChest, Main main) {
        this.treasureChest = treasureChest;
        this.chestLocation = treasureChest.getChestLoc();
        this.main = main;
    }

    @Override
    public void run() {
        int endY = chestLocation.getBlockY();
        int currentY = chestLocation.getBlockY() + 20;
        List<Location> fireworkSpawns = new ArrayList<Location>();
        while (currentY >= endY) {
            Location currentLoc = new Location(chestLocation.getWorld(), chestLocation.getBlockX() + 0.5, currentY, chestLocation.getBlockZ() + 0.5);
            fireworkSpawns.add(currentLoc);
            currentY--;
        }
        FireworkTimer fireworkTimer = new FireworkTimer(main, fireworkSpawns, endY, treasureChest);
        Bukkit.getScheduler().runTaskAsynchronously(main, fireworkTimer);
        treasureChest.spawnChest();
    }

}
