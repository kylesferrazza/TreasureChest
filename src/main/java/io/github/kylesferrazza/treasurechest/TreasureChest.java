package io.github.kylesferrazza.treasurechest;

import io.github.kylesferrazza.treasurechest.Listeners.MoveListener;
import net.minecraft.server.v1_8_R1.BlockPosition;
import net.minecraft.server.v1_8_R1.Blocks;
import net.minecraft.server.v1_8_R1.PacketPlayOutBlockAction;
import org.bukkit.*;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.DirectionalContainer;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by Kyle Sferrazza on 11/2/2014.
 * This file is a part of: TreasureChest.
 * All rights reserved.
 */
public class TreasureChest {
    private Main main;
    private Location playerLoc, beaconLoc, chestLoc, glassLoc, firstDiamond, itemDropLoc;
    private List<Location> goldBlocks = new ArrayList<Location>();
    private List<Location> emeraldBlocks = new ArrayList<Location>();
    private List<Location> lampBlocks = new ArrayList<Location>();
    private List<Location> fenceBlocks = new ArrayList<Location>();
    private List<Location> redstoneBlocks = new ArrayList<Location>();
    private List<Location> airBlocks = new ArrayList<Location>();
    private List<Location> allLocs = new ArrayList<Location>();
    private Vector direction;
    private Player player;
    private Chest c;
    private Color fireworkColor = getRandomColor();
    private BlockFace chestDir;

    private int fireworkNum = 15;

    private List<BlockState> originalBlocks = new ArrayList<BlockState>();

    public TreasureChest(Main main, Player player) {
        this.main = main;
        this.player = player;
        playerLoc = new Location(player.getWorld(), player.getEyeLocation().getBlockX(), player.getLocation().getBlockY(), player.getEyeLocation().getBlockZ());
        Location blockInFront = playerLoc;
        direction = player.getLocation().getDirection();
        direction.setY(0);
        blockInFront = blockInFront.add(direction);

        double locX = blockInFront.getBlockX();
        double locY = blockInFront.getBlockY();
        double locZ = blockInFront.getBlockZ();
        World world = blockInFront.getWorld();

        chestLoc = new Location(world, locX, locY, locZ);
        itemDropLoc = new Location(world, locX + 0.5, locY + 1, locZ + 0.5);
        allLocs.add(chestLoc);
        locY--;
        glassLoc = new Location(world, locX, locY, locZ);
        allLocs.add(glassLoc);
        locY--;
        beaconLoc = new Location(world, locX, locY, locZ);
        allLocs.add(beaconLoc);
        locY--;
        locX--;
        locZ--;
        firstDiamond = new Location(world, locX, locY, locZ);
        allLocs.add(firstDiamond);

        /*EMERALD*/
        locX = blockInFront.getBlockX();
        locY = blockInFront.getBlockY();
        locZ = blockInFront.getBlockZ();

        locY--;
        locX++;
        locZ--;
        emeraldBlocks.add(new Location(world, locX, locY, locZ));
        locX -= 2;
        emeraldBlocks.add(new Location(world, locX, locY, locZ));
        locZ +=2;
        emeraldBlocks.add(new Location(world, locX, locY, locZ));
        locX +=2;
        emeraldBlocks.add(new Location(world, locX, locY, locZ));
        /*END EMERALD*/

        /*GOLD*/
        locX = blockInFront.getBlockX();
        locY = blockInFront.getBlockY();
        locZ = blockInFront.getBlockZ();

        locY--;
        locX--;
        goldBlocks.add(new Location(world, locX, locY, locZ));
        locX--;
        goldBlocks.add(new Location(world, locX, locY, locZ));
        locX += 3;
        goldBlocks.add(new Location(world, locX, locY, locZ));
        locX++;
        goldBlocks.add(new Location(world, locX, locY, locZ));
        locX -= 2;
        locZ--;
        goldBlocks.add(new Location(world, locX, locY, locZ));
        locZ--;
        goldBlocks.add(new Location(world, locX, locY, locZ));
        locZ += 3;
        goldBlocks.add(new Location(world, locX, locY, locZ));
        locZ++;
        goldBlocks.add(new Location(world, locX, locY, locZ));
        /*END GOLD*/

        /*LAMPS*/
        locX = blockInFront.getBlockX();
        locY = blockInFront.getBlockY();
        locZ = blockInFront.getBlockZ();
        locY--;
        locX -= 2;
        locZ ++;
        lampBlocks.add(new Location(world, locX, locY, locZ));
        locZ ++;
        lampBlocks.add(new Location(world, locX, locY, locZ));
        locX++;
        lampBlocks.add(new Location(world, locX, locY, locZ));

        locX += 2;
        lampBlocks.add(new Location(world, locX, locY, locZ));
        locX++;
        lampBlocks.add(new Location(world, locX, locY, locZ));
        locZ--;
        lampBlocks.add(new Location(world, locX, locY, locZ));

        locZ -= 2;
        lampBlocks.add(new Location(world, locX, locY, locZ));
        locZ--;
        lampBlocks.add(new Location(world, locX, locY, locZ));
        locX--;
        lampBlocks.add(new Location(world, locX, locY, locZ));

        locX -= 2;
        lampBlocks.add(new Location(world, locX, locY, locZ));
        locX--;
        lampBlocks.add(new Location(world, locX, locY, locZ));
        locZ++;
        lampBlocks.add(new Location(world, locX, locY, locZ));
        /*END LAMPS*/

        for (Location loc : lampBlocks) {
            fenceBlocks.add(new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY() + 1, loc.getBlockZ()));
            redstoneBlocks.add(new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY() - 1, loc.getBlockZ()));
            airBlocks.add(new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY() + 2, loc.getBlockZ()));
        }
        for (Location loc : emeraldBlocks) {
            airBlocks.add(new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY() + 1, loc.getBlockZ()));
            airBlocks.add(new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY() + 2, loc.getBlockZ()));
        }
        for (Location loc : goldBlocks) {
            airBlocks.add(new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY() + 1, loc.getBlockZ()));
            airBlocks.add(new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY() + 2, loc.getBlockZ()));
        }
        airBlocks.add(new Location(chestLoc.getWorld(), chestLoc.getBlockX(), chestLoc.getBlockY() + 1, chestLoc.getBlockZ()));


        for (Location loc : emeraldBlocks) {
            allLocs.add(loc);
        }
        for (Location loc : goldBlocks) {
            allLocs.add(loc);
        }
        for (Location loc : lampBlocks) {
            allLocs.add(loc);
        }
        for (Location loc : fenceBlocks) {
            allLocs.add(loc);
        }
        for (Location loc : redstoneBlocks) {
            allLocs.add(loc);
        }
        for (Location loc : airBlocks) {
            allLocs.add(loc);
        }

        if (!Interacts.locsGood(allLocs)) {
            player.sendMessage(ChatColor.RED + "You are too close to another TreasureChest!");
            return;
        }

        for (Location loc : emeraldBlocks) {
            originalBlocks.add(loc.getBlock().getState());
            loc.getBlock().setType(Material.EMERALD_BLOCK);
        }

        for (Location loc : goldBlocks) {
            originalBlocks.add(loc.getBlock().getState());
            loc.getBlock().setType(Material.GOLD_BLOCK);
        }

        for (Location loc : lampBlocks) {
            originalBlocks.add(loc.getBlock().getState());
            loc.getBlock().setType(Material.REDSTONE_LAMP_ON);
        }

        for (Location loc : fenceBlocks) {
            originalBlocks.add(loc.getBlock().getState());
            loc.getBlock().setType(Material.NETHER_FENCE);
        }

        for (Location loc : redstoneBlocks) {
            originalBlocks.add(loc.getBlock().getState());
            loc.getBlock().setType(Material.REDSTONE_BLOCK);
        }

        for (Location loc : airBlocks) {
            originalBlocks.add(loc.getBlock().getState());
            loc.getBlock().setType(Material.AIR);
        }

        originalBlocks.add(beaconLoc.getBlock().getState());
        originalBlocks.add(chestLoc.getBlock().getState());
        originalBlocks.add(glassLoc.getBlock().getState());

        chestLoc.getBlock().setType(Material.AIR);

        for (double x = 0; x < 3; x++) {
            for (double z = 0; z < 3; z++) {
                Location currentDiamond = new Location(firstDiamond.getWorld(), firstDiamond.getX() + x, firstDiamond.getY(), firstDiamond.getZ() + z);
                originalBlocks.add(currentDiamond.getBlock().getState());
                currentDiamond.getBlock().setType(Material.DIAMOND_BLOCK);
            }
        }

        beaconLoc.getBlock().setType(Material.BEACON);

        spawnRandomColoredGlass(glassLoc);

        Interacts.addChest(this);
        MoveListener.disallowMovement(player);

        locX = chestLoc.getBlockX();
        locY = chestLoc.getBlockY();
        locZ = chestLoc.getBlockZ();
        locX += 0.5;
        locZ += 0.5;

        chestDir = BlockFace.valueOf(getCardinalDirection(player));

        float yaw = 0;
        String cardDir = getCardinalDirection(player);
        if (cardDir.equals("SOUTH")) {
            yaw = 180;
        } else if (cardDir.equals("WEST")) {
            yaw = -90;
        } else if (cardDir.equals("NORTH")) {
            yaw = 0;
        } else if (cardDir.equals("EAST")) {
            yaw = 90;
        }
        Location toTP = new Location(world, locX, locY, locZ, yaw, 0F);
        toTP.subtract(direction);
        player.teleport(toTP);


        Bukkit.getServer().getScheduler().runTask(main, new ChestSpawnEffect(this, main));
    }

    @SuppressWarnings("deprecation")
    public void spawnRandomColoredGlass(Location block) {
        block.getBlock().setType(Material.STAINED_GLASS);
        block.getBlock().setData(colors.get(fireworkColor));
    }

    public void spawnChest() {
        chestLoc.getBlock().setType(Material.CHEST);
        c = (Chest) chestLoc.getBlock().getState();
        DirectionalContainer data = (DirectionalContainer) c.getData();
        data.setFacingDirection(chestDir);
        c.setData(data);
        c.update();
        Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Timer(this), 150L);
    }

    public void endCycle() {
        MoveListener.allowMovement(player);

        for (BlockState blockState : originalBlocks) {
            blockState.update(true, false);
        }
        Interacts.removeChest(this);
    }

    public static String getCardinalDirection(Player player) {
        double rotation = (player.getLocation().getYaw()) % 360;
        if (rotation < 0) {
            rotation += 360.0;
        }
        if (0 <= rotation && rotation < 45) {
            return "NORTH";
        } else if (45 <= rotation && rotation < 135) {
            return "EAST";
        } else if (135 <= rotation && rotation < 225) {
            return "SOUTH";
        } else if (225 <= rotation && rotation < 315) {
            return "WEST";
        } else if (315 <= rotation && rotation < 360.0) {
            return "NORTH";
        } else {
            return null;
        }
    }

    public Location getChestLoc() {
        return chestLoc;
    }

    public static HashMap<Location, Integer> tasks = new HashMap<Location, Integer>();

    public void openChest(final Location loc) {
        if (!(loc.getBlock().getState() instanceof Chest)) {
            return;
        }
        final PacketPlayOutBlockAction packet = new PacketPlayOutBlockAction( new BlockPosition(loc.getX(), loc.getY(), loc.getZ()), Blocks.CHEST, 1, 1);

        tasks.put(loc,Bukkit.getScheduler().scheduleSyncRepeatingTask(main, new Runnable() {
            public void run() {
                if (!(loc.getBlock().getState() instanceof Chest)) {
                    Bukkit.getServer().getScheduler().cancelTask(tasks.get(loc));
                    tasks.remove(loc);
                    return;
                }
                for (Player p : loc.getWorld().getPlayers()) {
                    ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
                }
            }
        }, 0L, 55L));
        loc.getWorld().playSound(loc, Sound.CHEST_OPEN, 1F, 1F);

        final ItemStack itemStack = new ItemStack(Material.DIAMOND, 1);
        ItemMeta meta = itemStack.getItemMeta();
        meta.setDisplayName("Gadget");
        List<String> lore = new ArrayList<String>();
        lore.add("This is a gadget.");
        meta.setLore(lore);
        itemStack.setItemMeta(meta);

        final Item item = itemDropLoc.getWorld().dropItem(itemDropLoc, itemStack);

        item.setVelocity(direction.multiply(-0.12));
        item.setPickupDelay(10000);
        Bukkit.getScheduler().runTaskLater(main, new Runnable() {
            @Override
            public void run() {
                item.remove();
                player.getInventory().addItem(itemStack);
                player.updateInventory();
            }
        }, 70L);
    }

    public static final HashMap<Color, Byte> colors = new HashMap<Color, Byte>();

    static {
        colors.put(Color.WHITE, (byte) 0);
        colors.put(Color.ORANGE, (byte) 1);
        colors.put(Color.FUCHSIA, (byte) 2);
        colors.put(Color.fromRGB(0, 100, 255), (byte) 3);
        colors.put(Color.YELLOW, (byte) 4);
        colors.put(Color.LIME, (byte) 5);
        colors.put(Color.fromRGB(255, 75, 255), (byte) 6);
        colors.put(Color.GRAY, (byte) 7);
        colors.put(Color.fromRGB(193, 193, 193), (byte) 8);
        colors.put(Color.fromRGB(0, 255, 255), (byte) 9);
        colors.put(Color.PURPLE, (byte) 10);
        colors.put(Color.BLUE, (byte) 11);
        colors.put(Color.fromRGB(150, 75, 0), (byte) 12);
        colors.put(Color.GREEN, (byte) 13);
        colors.put(Color.RED, (byte) 14);
        colors.put(Color.BLACK, (byte) 15);
    }

    public static Color getRandomColor() {
        ArrayList<Color> colorslist = new ArrayList<Color>(colors.keySet());
        Random rand = new Random();
        return colorslist.get(rand.nextInt(colorslist.size()));
    }

    public Color getFireworkColor() {
        return fireworkColor;
    }

    public List<Location> getOriginalBlockLocs() {
        List<Location> original = new ArrayList<Location>();
        for (BlockState state : originalBlocks) {
            original.add(state.getLocation());
        }
        return original;
    }

    public List<Location> getAllLocs() {
        return allLocs;
    }
}
