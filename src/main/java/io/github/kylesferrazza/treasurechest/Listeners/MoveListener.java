package io.github.kylesferrazza.treasurechest.Listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kyle Sferrazza on 11/15/2014.
 * This file is a part of: TreasureChest.
 * All rights reserved.
 */
public class MoveListener implements Listener {

    private static List<Player> noMove = new ArrayList<Player>();

    public static void disallowMovement(Player p) {
        if (!(noMove.contains(p))) {
            noMove.add(p);
        }
    }
    public static void allowMovement(Player p) {
        if (noMove.contains(p)) {
            noMove.remove(p);
        }
    }

    @EventHandler
    public void moveHandler(PlayerMoveEvent event) {
        if(noMove.contains(event.getPlayer())) {
            if ((event.getFrom().getX() == event.getTo().getX()) && (event.getFrom().getY() == event.getTo().getY()) && (event.getFrom().getZ() == event.getTo().getZ())) {
                return;
            }
            event.setTo(event.getFrom());
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (noMove.contains(e.getPlayer())) {
            e.setCancelled(true);
        }
    }

    public static boolean playerCanMove(Player player) {
        return !noMove.contains(player);
    }
}
