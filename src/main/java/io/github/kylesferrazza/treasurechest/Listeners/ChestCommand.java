package io.github.kylesferrazza.treasurechest.Listeners;

import io.github.kylesferrazza.treasurechest.Main;
import io.github.kylesferrazza.treasurechest.TreasureChest;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Kyle Sferrazza on 11/2/2014.
 * This file is a part of: TreasureChest.
 * All rights reserved.
 */
public class ChestCommand implements CommandExecutor {

    Main main;

    public ChestCommand(Main main) {
        this.main = main;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player)) {
            commandSender.sendMessage("You cannot use this plugin from the console.");
            return true;
        }

        Player player = (Player) commandSender;

        if (!player.isOnGround()) {
            player.sendMessage(ChatColor.RED + "You are not on the ground!");
            return true;
        }

        if (!MoveListener.playerCanMove(player)) {
            player.sendMessage(ChatColor.RED + "You already have a treasure chest in progress!");
            return true;
        }

        if (player.getLocation().getBlockY() < 4) {
            player.sendMessage(ChatColor.RED + "You have to be above a Y level of 4.");
            return true;
        }

        if (player.getLocation().getBlockY() >= 254) {
            player.sendMessage(ChatColor.RED + "You have to be below a Y level of 254.");
            return true;
        }

        TreasureChest t = new TreasureChest(main, player);

        return false;
    }
}
