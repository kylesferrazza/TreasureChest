package io.github.kylesferrazza.treasurechest;

/**
 * Created by Kyle on 12/6/2014.
 */
public class ChestSpawner implements Runnable {
    private TreasureChest treasureChest;

    public ChestSpawner (TreasureChest treasureChest) {
        this.treasureChest = treasureChest;
    }

    @Override
    public void run() {
        treasureChest.openChest(treasureChest.getChestLoc());
    }
}
