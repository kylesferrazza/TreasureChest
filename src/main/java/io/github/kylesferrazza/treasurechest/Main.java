package io.github.kylesferrazza.treasurechest;

import io.github.kylesferrazza.treasurechest.Listeners.ChestCommand;
import io.github.kylesferrazza.treasurechest.Listeners.MoveListener;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Kyle Sferrazza on 11/2/2014.
 * This file is a part of: TreasureChest.
 * All rights reserved.
 */
public class Main extends JavaPlugin {

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(new MoveListener(), this);
        getServer().getPluginManager().registerEvents(new Interacts(), this);
        ChestCommand cc = new ChestCommand(this);
        getCommand("treasurechest").setExecutor(cc);
    }
}
